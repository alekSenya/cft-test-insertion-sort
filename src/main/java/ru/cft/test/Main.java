package ru.cft.test;

import org.apache.commons.cli.ParseException;
import ru.cft.test.config.Config;
import ru.cft.test.config.Configuration;
import ru.cft.test.filework.FileWorker;
import ru.cft.test.sort.InsertionSort;
import ru.cft.test.utils.Utils;

public class Main {
    public static void main(String[] args) {
        try {
            Config config = new Configuration(args).getConfig();
            String[] originalArr = FileWorker.read(config.getInputFile());
            switch (config.getFileType()) {
                case INTEGER:
                    Integer[] integerArr = Utils.convertArrayStringToArrayInteger(originalArr);
                    Integer[] sortedIntegerArr = new Integer[integerArr.length];
                    switch (config.getOrderType()) {
                        case ASC:
                            sortedIntegerArr = new InsertionSort<Integer>().insertionSort(integerArr, Integer::compareTo);
                            break;
                        case DESC:
                            sortedIntegerArr = new InsertionSort<Integer>()
                                    .insertionSort(integerArr, (i1, i2) -> - i1.compareTo(i2));
                            break;
                    }
                    FileWorker.write(sortedIntegerArr, config.getOutputFile());
                    break;
                case STRING:
                    String[] sortedStringArr = null;
                    switch (config.getOrderType()) {
                        case ASC:
                            sortedStringArr = new InsertionSort<String>().insertionSort(originalArr, String::compareTo);
                            break;
                        case DESC:
                            sortedStringArr = new InsertionSort<String>().insertionSort(originalArr, (s1, s2) -> - s1.compareTo(s2));
                            break;
                    }
                    FileWorker.write(sortedStringArr, config.getOutputFile());
                    break;
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            Configuration.printHelp();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}