package ru.cft.test.config;

/**
 * Класс для сохранения праматров запуска программы
 */
public final class Config {
    private final String inputFile;
    private final String outputFile;
    private final FileType fileType;
    private final OrderType orderType;

    Config(String inputFile, String outputFile, FileType fileType, OrderType orderType) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        this.fileType = fileType;
        this.orderType = orderType;
    }

    public String getInputFile() {
        return inputFile;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public FileType getFileType() {
        return fileType;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    @Override
    public String toString() {
        return "Config{" +
                "inputFile='" + inputFile + '\'' +
                ", outputFile='" + outputFile + '\'' +
                ", fileType='" + fileType + '\'' +
                ", orderType='" + orderType + '\'' +
                '}';
    }
}
