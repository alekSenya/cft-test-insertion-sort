package ru.cft.test.config;

import org.apache.commons.cli.*;

/**
 * Класс отвечающий за конфигурацию программы
 */
public final class Configuration {
    private static final Options options = new Options()
            .addOption("h", "help", false, "Выводит возможные парметры запуска")
            .addOption(null, "input-file",true, "Путь к исходному файлу (Обязательный параметр)")
            .addOption(null, "output-file", true, "Путь к результирующему файлу (Обязательный параметр)")
            .addOption("i", false, "Указатель на тип данных исходного файла (Обязательный параметр. " +
                    "Необходимо обязательно указать один из типов исходного файла. " +
                    "При указании данного параметра, тип данных исходного файла будет integer)")
            .addOption("s", false, "Указатель на тип данных исходного файла (Обязательный параметр. " +
                    "Необходимо обязательно указать один из типов исходного файла." +
                    "При указании данного параметра, тип данных исходного файла будет string)")
            .addOption("a", false, "Указатель на тип сортировки (Обязательный параметр. " +
                    "Необходимо обязательно указать один из типов сортировки. " +
                    "При указании данного параметра, тип сортировки будет ASC)")
            .addOption("d", false, "Указатель на тип сортировки (Обязательный параметр. " +
                    "Необходимо обязательно указать один из типов сортировки. " +
                    "При указании данного параметра, тип сортировки будет DESC)");
    private final Config config;

    /**
     * Конструктор класса в который передается массив String[] аргументов коммандной строки
     * @param args - массив аргументов коммандной строки
     * @throws ParseException
     * Если ввели команду -h
     * Если не указан путь к исходному файлу
     * Если не указан путь к результирующему файлу
     * Если не указан тип данных исходного файла или указаны сразу несколько
     * Если не указан тип сортировки или указаны сразу несколько
     */
    public Configuration(String[] args) throws ParseException {
        CommandLine commandLine = new DefaultParser().parse(options, args);
        if (commandLine.hasOption("h")) {
            throw new ParseException("Подсказка по возможным параметрам.");
        }

        String inputFile = commandLine.getOptionValue("input-file");
        String outputFile = commandLine.getOptionValue("output-file");
        FileType fileType = null;
        OrderType orderType = null;
        if (inputFile != null) {
            if (outputFile != null) {
                //Тип исходного файла
                if (commandLine.hasOption("i") && !commandLine.hasOption("s")) {
                    fileType = FileType.INTEGER;
                } else if (!commandLine.hasOption("i") && commandLine.hasOption("s")) {
                    fileType = FileType.STRING;
                }
                if (fileType == null) {
                    throw new ParseException("Не указан тип данных исходного файла или указаны сразу несколько.");
                }

                //Тип сортировки
                if (commandLine.hasOption("a") && !commandLine.hasOption("d")) {
                    orderType = OrderType.ASC;
                } else if (!commandLine.hasOption("a") && commandLine.hasOption("d")) {
                    orderType = OrderType.DESC;
                }
                if (orderType == null) {
                    throw new ParseException("Не указан тип сортировки или указаны сразу несколько.");
                }

                config = new Config(inputFile, outputFile, fileType, orderType);
            } else {
                throw new ParseException("Не указан путь к результирующему файлу.");
            }
        } else {
            throw new ParseException("Не указан путь к исходному файлу.");
        }
    }

    /**
     * Метод для получения конфигурации запуска
     * @return конфигурацию запуска
     */
    public Config getConfig() {
        return config;
    }

    /**
     * Метод для выводв подсказки по возможным параметрам запуска программы
     */
    public static void printHelp() {
        new HelpFormatter().printHelp(100, " ", null, options, null);
    }
}
