package ru.cft.test.config;

/**
 * Перечисление, хранящее тип содержимого исходного файла
 */
public enum FileType { INTEGER, STRING }
