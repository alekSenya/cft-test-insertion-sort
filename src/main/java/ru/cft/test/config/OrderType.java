package ru.cft.test.config;

/**
 * Перечисление, хранящее тип сортировки
 */
public enum OrderType { ASC, DESC }