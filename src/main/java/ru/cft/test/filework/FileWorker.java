package ru.cft.test.filework;

import java.io.*;

/**
 * Класс для работы с файлами (чтение/запись)
 */
public class FileWorker {
    /**
     * Метод для считывания строк в файле в String[]
     * @param file - путь до файла, который необходимо прочитать
     * @return String[] - массив строк файла
     * @throws IOException
     * - Если в файле более 100 строк.
     * - Если в файле присутвуют пробелы.
     * - Если файл не был найден.
     */
    public static String[] read(String file) throws IOException {
        String[] arr = new String[100];
        int i = 0;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String fileLine;
            while ((fileLine = br.readLine()) != null) {
                if (i >= 100) {
                    throw new IOException("В файле слишком много строк. Сделайте кол-во строк <= 100.");
                }
                if (fileLine.contains(" ")) {
                    throw new IOException("В строках файла присутствуют символы пробела.");
                }
                arr[i++] = fileLine;
            }
        } catch (FileNotFoundException e) {
            throw new IOException("Файл не найден.");
        }
        String[] resultArr = new String[i];
        System.arraycopy(arr, 0, resultArr, 0, i);
        return resultArr;
    }

    /**
     * Метод для запись массива объектов в файл
     * @param arr Object[] - массив объектов, для записи в файл
     * @param outputFile - путь до файла в который будет осуществляться запись
     * @throws IOException
     * Если произошла ошибка во время записи в файл
     */
    public static void write(Object[] arr, String outputFile) throws IOException {
        File file = new File(outputFile);
        try {
            file.createNewFile();
            try (PrintWriter out = new PrintWriter(file.getAbsoluteFile())) {
                for (Object aText : arr) out.println(aText);
            }
        } catch (IOException e) {
            throw new IOException("Непредвиденная ошибка вызванная, вероятней всего, невозможностью создать файл " + outputFile);
        }
    }
}
