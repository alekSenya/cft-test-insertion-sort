package ru.cft.test.sort;

import java.util.Comparator;

/**
 * Класс отвечающий за сортировку вставками
 * @param <T> - тип массива для сортировки
 */
public class InsertionSort<T> {
    /**
     * Метод реализующий сортировку вствками
     * @param arr - массив, который необходимо отсортировать
     * @param c - в метод передается Comparator, который указывает каким образом сравнивать элементы массива
     * @return отсортированный массив
     */
    public T[] insertionSort(T[] arr, Comparator<T> c) {
        T temp;
        int j;
        for(int i = 0; i < arr.length - 1; i++) {
            if (c.compare(arr[i], arr[i + 1]) > 0) {
                temp = arr[i + 1];
                arr[i + 1] = arr[i];
                j = i;
                while (j > 0 && c.compare(temp, arr[j - 1]) < 0) {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = temp;
            }
        }
        return arr;
    }
}
