package ru.cft.test.utils;

/**
 * Класс содержащий вспомогательные методы
 */
public final class Utils {
    /**
     * Метод конвертирующий массив строк в массив целых чисел
     * @param strings - массив строк, который необходимо преобразовать
     * @return Integer[] - массив целых чисел, полученный из строк
     */
    public static Integer[] convertArrayStringToArrayInteger(String[] strings) {
        Integer[] intArray = new Integer[strings.length];
        int i=0;
        for(String str:strings){
            try {
                intArray[i]=Integer.parseInt(str);
                i++;
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Содержимое файла не соответвует integer.");
            }
        }
        return intArray;
    }
}
