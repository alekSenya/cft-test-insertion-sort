package ru.cft.test.config;

import org.apache.commons.cli.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConfigurationTest {

    @Test
    public void testCorrectParameters() {
        String[] correctParameters = {"--input-file", "in.txt", "--output-file", "out.txt", "-i", "-a"};
        try {
            Config config = new Configuration(correctParameters).getConfig();
            assertTrue(config != null);
            assertTrue(config.getInputFile() != null);
            assertTrue(config.getOutputFile() != null);
            assertTrue(config.getFileType() != null);
            assertTrue(config.getOrderType() != null);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSeveralOrderTypesParameters() {
        String[] severalOrderTypesParameters = {"--input-file", "in.txt", "--output-file", "out.txt", "-i", "-d", "-a"};
        try {
            new Configuration(severalOrderTypesParameters);
        } catch (ParseException e) {
            assertEquals("Не указан тип сортировки или указаны сразу несколько.", e.getMessage());
        }
    }

    @Test
    public void testSeveralFileTypesParameters() {
        String[] severalFileTypesParameters = {"--input-file", "in.txt", "--output-file", "out.txt", "-i", "-s", "-a"};
        try {
            new Configuration(severalFileTypesParameters);
        } catch (ParseException e) {
            assertEquals("Не указан тип данных исходного файла или указаны сразу несколько.", e.getMessage());
        }
    }

    @Test
    public void testNoSpecifiedInputFileParameters() {
        String[] noSpecifiedInputFileParameters = {"--output-file", "out.txt", "-i", "-a"};
        try {
            new Configuration(noSpecifiedInputFileParameters);
        } catch (ParseException e) {
            assertEquals("Не указан путь к исходному файлу.", e.getMessage());
        }
    }

    @Test
    public void testNoSpecifiedOutputFileParameters() {
        String[] noSpecifiedOutputFileParameters = {"--input-file", "in.txt", "-i", "-a"};
        try {
            new Configuration(noSpecifiedOutputFileParameters);
        } catch (ParseException e) {
            assertEquals("Не указан путь к результирующему файлу.", e.getMessage());
        }
    }

    @Test
    public void testNoFileTypeSpecifiedParameters() {
        String[] noFileTypeSpecifiedParameters = {"--input-file", "in.txt", "--output-file", "out.txt", "-a"};
        try {
            new Configuration(noFileTypeSpecifiedParameters);
        } catch (ParseException e) {
            assertEquals("Не указан тип данных исходного файла или указаны сразу несколько.", e.getMessage());
        }
    }

    @Test
    public void testNoOrderTypeSpecifiedParameters() {
        String[] noOrderTypeSpecifiedParameters = {"--input-file", "in.txt", "--output-file", "out.txt", "-i"};
        try {
            new Configuration(noOrderTypeSpecifiedParameters);
        } catch (ParseException e) {
            assertEquals("Не указан тип сортировки или указаны сразу несколько.", e.getMessage());
        }
    }

}
