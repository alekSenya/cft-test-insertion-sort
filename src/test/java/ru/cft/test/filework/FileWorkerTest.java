package ru.cft.test.filework;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class FileWorkerTest {
    private final String directory = "src/test/java/ru/cft/test/files/";

    @Test
    public void testReadFileNotFound() {
        String fileNotFound = "";
        try {
            FileWorker.read(directory + fileNotFound);
        } catch (IOException e) {
            assertEquals("Файл не найден.", e.getMessage());
        }
    }

    @Test
    public void testReadTooManyLines() {
        String tooManyLines = "tooManyLines.txt";
        try {
            FileWorker.read(directory + tooManyLines);
        } catch (IOException e) {
            assertEquals("В файле слишком много строк. Сделайте кол-во строк <= 100.", e.getMessage());
        }
    }

    @Test
    public void testReadSpaceCharacters() {
        String spaceCharacters = "spaceCharacters.txt";
        try {
            FileWorker.read(directory + spaceCharacters);
        } catch (IOException e) {
            assertEquals("В строках файла присутствуют символы пробела.", e.getMessage());
        }
    }

    @Test
    public void testWriteFileNotFound() {
        try {
            FileWorker.write(new String[0], "/test/test/test.txt");
        } catch (IOException e) {
            assertEquals("Непредвиденная ошибка вызванная, вероятней всего, невозможностью создать файл /test/test/test.txt", e.getMessage());
        }
    }
}
