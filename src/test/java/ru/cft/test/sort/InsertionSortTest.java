package ru.cft.test.sort;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class InsertionSortTest {
    @Test
    public void testIntegerInsertionSort() {
        Integer[] sortedArr = new InsertionSort<Integer>().insertionSort(new Integer[]{1, 7, 0}, Integer::compareTo);
        assertTrue(Arrays.equals(sortedArr, new Integer[]{0, 1, 7}));

        sortedArr = new InsertionSort<Integer>().insertionSort(new Integer[]{1, 7, 0}, (i1, i2) -> - i1.compareTo(i2));
        assertTrue(Arrays.equals(sortedArr, new Integer[]{7, 1, 0}));
    }

    @Test
    public void testStringInsertionSort() {
        String[] sortedArr = new InsertionSort<String>().insertionSort(new String[]{"a", "b", "ab", "ac"}, String::compareTo);
        assertTrue(Arrays.equals(sortedArr, new String[]{"a", "ab", "ac", "b"}));

        sortedArr = new InsertionSort<String>().insertionSort(new String[]{"a", "b", "ab", "ac"},(s1, s2) -> - s1.compareTo(s2));
        assertTrue(Arrays.equals(sortedArr, new String[]{"b", "ac", "ab", "a"}));

        sortedArr = new InsertionSort<String>().insertionSort(new String[]{"1", "2", "11", "13"}, String::compareTo);
        assertTrue(Arrays.equals(sortedArr, new String[]{"1", "11", "13", "2"}));
    }
}
